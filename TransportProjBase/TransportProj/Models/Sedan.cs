﻿using System;
using TransportProj.Models;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveTo(int x, int y)
        {
            if (XPos < x)
            {
                MoveRight();
                WritePositionToConsole();
                return;
            }

            if (XPos > x)
            {
                MoveLeft();
                WritePositionToConsole();
                return;
            }

            if (YPos > y)
            {
                MoveDown();
                WritePositionToConsole();
                return;
            }

            if (YPos < y)
            {
                MoveUp();
                WritePositionToConsole();
                return;
            }
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos++;
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos--;
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos++;
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos--;
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan '" +Name +"' moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
