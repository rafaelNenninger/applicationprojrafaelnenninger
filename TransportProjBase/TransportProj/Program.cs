﻿using System;
using TransportProj.Models;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;
            int MaxTickCount = 50;

            var cityPosition = new CityPosition();
            City myCity = new City(CityLength, CityWidth);

            Passenger passenger = myCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            passenger.IsInARush = false;
            passenger.Name = "Nick";

            cityPosition.AddPassenger(passenger);

            var carFactory = new CarFactory(myCity);
            var car = carFactory.GetCarByUrgency(passenger.IsInARush, rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            car.Name = "c1";

            Driver driver = new Driver();
            driver.Name = "d1";
            car.AddDriverToCar(driver);
            car.Driver.SetAsAvailable();
            
            var car2 = carFactory.GetCarByUrgency(passenger.IsInARush, rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            car2.Name = "c2";

            Driver driver2 = new Driver();
            driver2.Name = "d2";
            car2.AddDriverToCar(driver2);
            car2.Driver.SetAsAvailable();

            cityPosition.AddCar(car);
            cityPosition.AddCar(car2);
            var tickCount = 0;

            var cityContext = new CityContext(myCity, cityPosition);

            while(!passenger.IsAtDestination() && tickCount < MaxTickCount)
            {
                Tick(cityContext);
                tickCount++;

                WritePassengerPosition(car, passenger);
            }

            WriteMessageToConsole("Tick count:" + tickCount);
            WriteMessageToConsole("Process completed. Hit any key....");
            var line = Console.ReadLine();

            return;
        }

        private static void WritePassengerPosition(Car car, Passenger passenger)
        {
            if (car.Driver.CurrentState == DriverState.DeliveringPassenger)
            {
                WriteMessageToConsole(String.Format("Passenger {0} destination x - {1} y - {2}", passenger.Name, passenger.DestinationXPos.ToString(), passenger.DestinationYPos.ToString()));
            }
            else
            {
                WriteMessageToConsole(String.Format("Passenger {0} location x - {1} y - {2}", passenger.Name, passenger.GetCurrentXPos(), passenger.GetCurrentYPos()));
            }

            if (car.HasPassenger && car.Passenger.IsAtDestination())
            {
                WriteMessageToConsole(string.Format("Passenger {0} is at destination", passenger.Name));
            }
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="cityContext">The City Context where cars and passengers are located and moving</param>
        private static void Tick(CityContext cityContext)
        {
            cityContext.ProcessCycle();
        }


        private static void WriteMessageToConsole(string message)
        {
            Console.WriteLine(message);
        }
        
    }
}
