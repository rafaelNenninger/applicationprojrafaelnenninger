﻿using System;

namespace TransportProj.Models
{
    public enum DriverState
    {
        Available,
        PickingUpPassenger,
        InFrontOfPassenger,
        DeliveringPassenger,
        DroppingPassenger
    }

    public class Driver
    {
        public string Name { get; set; }  
        public Car Car { get; set; }
        public DriverState CurrentState { get; private set; }
        public Passenger PassengerToPickup { get; set; }

        public bool HasPassengerToPickup
        {
            get
            {
                return PassengerToPickup != null;
            }
        }
        

        public Driver()
        {
            CurrentState = DriverState.Available;
        }

        public void GetInCar(Car car)
        {
            Car = car;
            WriteMessageToConsole("Driver " + Name +" got in car '" +car.Name +"'.");
        }

        public void SetForPassengerPickup(Passenger passenger)
        {
            if (passenger == null) throw new Exception("Assign a passenger to driver " + Name+" before setting for pickup");

            PassengerToPickup = passenger;
            CurrentState = DriverState.PickingUpPassenger;
        }

        public void SetForPassengerDelivery()
        {
            CurrentState = DriverState.DeliveringPassenger;
        }

        public void SetAsAvailable()
        {
            PassengerToPickup = null;
            CurrentState = DriverState.Available;
            WriteMessageToConsole("Driver " + Name + " is available");
        }

        public void Move()
        {
            if (CurrentState == DriverState.PickingUpPassenger && HasPassengerToPickup)
            {
                Car.MoveTo(PassengerToPickup.StartingXPos, PassengerToPickup.StartingYPos);
            }

            if (CurrentState == DriverState.DeliveringPassenger)
            {
                Car.MoveTo(Car.Passenger.DestinationXPos, Car.Passenger.DestinationYPos);
            }

            if (Car.IsAtPassengerLocation(PassengerToPickup) && PassengerToPickup.Status == PassengerStatus.WaitingForPickup)
            {
                CurrentState = DriverState.InFrontOfPassenger;
            }
        }

        public void WriteMessageToConsole(string message)
        {
            Console.WriteLine(message);
        }

    }
}
