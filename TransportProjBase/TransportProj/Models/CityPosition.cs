﻿using System.Collections.Generic;

namespace TransportProj.Models
{
    public class CityPosition
    {
        private List<Car> _cars = new List<Car>();
        private List<Passenger> _passengers = new List<Passenger>();

        public IList<Car> Cars
        {
            get
            {
                return _cars.AsReadOnly();
            }
        }

        public IList<Passenger> Passengers
        {
            get
            {
                return _passengers.AsReadOnly();
            }
        }

        public Passenger Passenger { get; set; }

        public CityPosition()
        {
        }

        public void AddCar(Car car)
        {
            _cars.Add(car);

        }

        public void AddPassenger(Passenger passenger)
        {
            _passengers.Add(passenger);
        }
    }
}
