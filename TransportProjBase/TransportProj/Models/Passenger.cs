﻿using System;

namespace TransportProj.Models
{
    public enum PassengerStatus
    {
        WaitingForPickup,
        BeingDelivered,
        Delivered
    }

    public class Passenger
    {
        public string Name { get; set; }
        public int StartingXPos { get; private set; }
        public int StartingYPos { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public Car Car { get; set; }
        public City City { get; private set; }
        public PassengerStatus Status { get; private set; }
        public bool IsInARush { get; set; }

        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
            StartingXPos = startXPos;
            StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;
            Status = PassengerStatus.WaitingForPickup;
        }

        public void GetInCar(Car car)
        {
            Car = car;
            Status = PassengerStatus.BeingDelivered;
            WriteMessageToConsole("Passenger " +Name +" got in car.");
        }

        public void GetOutOfCar()
        {
            Status = PassengerStatus.Delivered;
            Car = null;
        }

        public int GetCurrentXPos()
        {
            if(Car == null)
            {
                return StartingXPos;
            }
            else
            {
                return Car.XPos;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return StartingYPos;
            }
            else
            {
                return Car.YPos;
            }
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestinationXPos && GetCurrentYPos() == DestinationYPos;
        }

        public void WriteMessageToConsole(string message)
        {
            Console.WriteLine(message);
        }
    }
}
