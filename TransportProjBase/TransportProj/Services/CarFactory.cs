﻿using TransportProj.Models;

namespace TransportProj
{
    public class CarFactory
    {
        City City { get; set; }

        public CarFactory(City city)
        {
            City = city;
        }

        public Car GetCarByUrgency(bool isUrgent, int x, int y)
        {
            if (isUrgent) return new RaceCar(x, y, City, null);

            return new Sedan(x, y, City, null);
        }
    }
}
