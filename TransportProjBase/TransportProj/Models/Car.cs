﻿using System;

namespace TransportProj.Models
{
    public abstract class Car
    {
        public string Name { get; set; }
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        public Driver Driver { get; private set; }

        public bool HasPassenger
        {
            get
            {
                return (Passenger != null);
            }
        }

        

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void AddDriverToCar(Driver driver)
        {
            if (Driver != null) throw new Exception("Car already has a driver:" +Driver.Name);

            Driver = driver;
            Driver.GetInCar(this);
        }

        public bool IsAtPassengerLocation(Passenger passenger)
        {
            return (XPos == passenger.StartingXPos && YPos == passenger.StartingYPos);
        }

        public void GetPassengerInCar(Passenger passenger)
        {
            Passenger = passenger;
            Passenger.GetInCar(this);
            Driver.SetForPassengerDelivery();
        }

        public abstract void MoveTo(int x, int y);

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();
    }
}
