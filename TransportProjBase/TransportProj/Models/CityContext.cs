﻿using System.Linq;

namespace TransportProj.Models
{
    public class CityContext
    {
        public City City { get; set; }
        public CityPosition CityPosition { get; set; }

        public CityContext(City city, CityPosition cityPosition)
        {
            City = city;
            CityPosition = cityPosition;
        }

        /// <summary>
        /// Process a cycle of moving cars one spot or pick up the passenger
        /// </summary>
        public void ProcessCycle()
        {
            foreach (var car in CityPosition.Cars)
            {
                if (car.Driver.CurrentState == DriverState.Available)
                {
                    // check if there's a passenger to pickup
                    var passengerWaiting = CityPosition.Passengers.Where(p => p.Status == PassengerStatus.WaitingForPickup).FirstOrDefault();

                    if (passengerWaiting != null)
                    {
                        car.Driver.SetForPassengerPickup(passengerWaiting);
                    }
                    break;
                }

                if (car.Driver.CurrentState == DriverState.InFrontOfPassenger)
                {
                    car.GetPassengerInCar(car.Driver.PassengerToPickup);
                    var currentDriver = car.Driver;

                    // set any driver trying to pickup that passenger as available
                    CityPosition.Cars.Where(c => c.Driver.PassengerToPickup == car.Driver.PassengerToPickup && c.Driver != currentDriver).ToList().ForEach(c => c.Driver.SetAsAvailable());

                    break;
                }

                car.Driver.Move();
            }
        }
    }
}
